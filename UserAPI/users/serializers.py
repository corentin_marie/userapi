from django.contrib.auth.models import User
from rest_framework import serializers

class UserSerializer(serializers.Serializer):
    """
    Serializer class for the `User` database model
    """
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(required=True, allow_blank=False, max_length=100)
    birthday = serializers.DateField(read_only=True)
    is_superuser = serializers.BooleanField(default=False)
    is_staff = serializers.BooleanField(default=False)

    def create(self, data):
        """
        Create and return a new `User` instance given the data
        """
        return User.objects.create(data)

    def update(self, instance, data):
        """
        Update and return and existing `User` instance given the data
        """
        instance.username = data.get('username', instance.username)
        instance.birthday = data.get('birthday', instance.birthday)
        instance.is_superuser = data.get('is_superuser', instance.is_superuser)
        instance.is_staff = data.get('is_staff', instance.is_staff)
        instance.save()
        return instance

class UserPreviewSerializer(serializers.Serializer):
    """
    Serializer class for the "user_list" view data
    """
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(required=True, allow_blank=False, max_length=100)
    birthday = serializers.DateField(read_only=True)

    
