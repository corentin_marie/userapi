import json
from django.http import HttpResponse,JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from users.models import User
from users.serializers import UserSerializer, UserPreviewSerializer

@csrf_exempt
def user_list(request):
    """
    List all users
    """
    if request.method == "GET":
        users_preview = (User.objects.values('id','username','birthday'))
        serializer = UserPreviewSerializer(users_preview, many=True)
        return JsonResponse(serializer.data, safe=False)


    elif request.method == "POST":
        data = JSONParser().parse(request)
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
    
    
def user_detail(request, pk):
    """
    Retrieve, update or delete a User
    """
    try:
        user = User.objects.get(pk=pk)
    except User.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == "GET":
        serializer = UserSerializer(user)
        return JsonResponse(serializer.data)

    elif request.method == "PUT":
        data = JSONParser().parse(request)
        serializer = UserSerializer(user, data=data)
        if serializer.is_valid:
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == "DELETE":
        user.delete()
        return HttpResponse(status=204)
    

def year_filter(request, year):
    """
    Return user born in the requested year
    """
    users_filtered = User.objects.filter(birthday__year=int(year))
    serializer = UserSerializer(users_filtered, many=True)
    return JsonResponse(serializer.data, safe=False)
