import datetime
import json

from django.test import TestCase
from django.urls import reverse

from .models import User

def create_user(username, birthday, is_superuser=False, is_staff=False):
    """
    Create a user given his `username` and his `birthday`. It is possible
    to specify if he is superuser or part of the staff

    @username : string
    @birthday : datetime.date object
    @is_superuser : boolean
    @is_staff : boolean
    """
    if type(birthday) != datetime.date:
        raise ValueError('{birthday} should be a datetime.date object')
    else:
        return User.objects.create(username=username, birthday=birthday, is_superuser=is_superuser, is_staff=is_staff)

class UserListViewTest(TestCase):

    def test_list_view_all_data_displayed(self):
        """
        Test that the number of users of displayed are the number of users stored
        """
        birthday_test1 = datetime.date(1980,1,1)
        birthday_test2 = datetime.date(1990,1,1)
        birthday_test3 = datetime.date(1980,1,1)
        create_user('user1', birthday_test1)
        create_user('user2', birthday_test2)
        create_user('user3', birthday_test3)
        response = self.client.get('/users/')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        # check if it displayed all data
        self.assertEqual(len(data), 3)
    
    def test_list_view_what_data_displayed(self):
        """
        Test that the data displayed in the list view are 'id', 'username', 'birthday'
        """
        birthday_test1 = datetime.date(1980,1,1)
        birthday_test2 = datetime.date(1990,1,1)
        birthday_test3 = datetime.date(1980,1,1)
        create_user('user1', birthday_test1)
        create_user('user2', birthday_test2)
        create_user('user3', birthday_test3)
        response = self.client.get('/users/')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        for user in data:
            # We check that each user contains 3 data
            self.assertEqual(len(user), 3)
            # We check that data displayed are 'id', 'username', 'birthday'
            self.assertNotEqual(user["id"], None)
            self.assertNotEqual(user["username"], None)
            self.assertNotEqual(user["birthday"], None)

class UserDetailViewTest(TestCase):

    def test_right_user_detail_data(self):
        """
        Check if the user id correspond to id requested
        """
        birthday_test1 = datetime.date(1980,1,1)
        create_user('user1', birthday_test1)
        response = self.client.get('/users/1/')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data["id"], 1)

    def test_detail_view_data_displayed(self):
        """
        Test if all data of users are displayed
        """
        birthday_test1 = datetime.date(1980,1,1)
        create_user('user1', birthday_test1)
        response = self.client.get('/users/1/')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(len(data), 5)
        self.assertNotEqual(data["id"], None)
        self.assertNotEqual(data["username"], None)
        self.assertNotEqual(data["birthday"], None)
        self.assertNotEqual(data["is_superuser"], None)
        self.assertNotEqual(data["is_staff"], None)

    def test_error_inexisting_user(self):
        """
        Test if an error occur if we search an inexisting user
        """
        birthday_test1 = datetime.date(1980,1,1)
        create_user('user1', birthday_test1)
        response = self.client.get('/users/2/')
        self.assertEqual(response.status_code, 404)

class UserYearFilterTest(TestCase):
    def test_birthday_user_requested(self):
        birthday_test1 = datetime.date(1980,1,1)
        birthday_test2 = datetime.date(1990,1,1)
        birthday_test3 = datetime.date(1980,1,1)
        create_user('user1', birthday_test1)
        create_user('user2', birthday_test2)
        create_user('user3', birthday_test3)
        response = self.client.get('/users/year=1980/')
        data = json.loads(response.content)
        self.assertEqual(len(data), 2)
        for user in data:
            birthday = datetime.datetime.strptime(user["birthday"], "%Y-%m-%d").date()
            self.assertEqual(birthday.year, 1980)


                



        