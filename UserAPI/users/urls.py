from django.urls import path, re_path
from django.conf.urls import url, include
from rest_framework import routers
from . import views

app_name='users'

urlpatterns = [
    #re_path(r'^users/(?P<year>[0-9]{4})/$', views.year_filter),
    url(r'^users/$', views.user_list),
    url(r'^users/(?P<pk>[0-9]+)/$', views.user_detail),
    re_path(r'^users/year=(?P<year>[0-9]{4})/$', views.year_filter),
]