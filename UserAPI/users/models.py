import datetime

from django.db import models

class User(models.Model):
    username = models.CharField(max_length=200)
    birthday = models.DateField(default=None)
    is_superuser = models.BooleanField(default=0)
    is_staff = models.BooleanField(default=0)

    def __str__(self):
        return self.username
    

